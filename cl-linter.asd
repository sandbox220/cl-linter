(in-package :asdf-user)

(defsystem :cl-linter
  :class :package-inferred-system
  :defsystem-depends-on (:asdf-package-system)
  :in-order-to ((test-op (test-op :cl-linter/tests)))
  :pathname #p"./src/")

(defsystem :cl-linter/tests
  :class :package-inferred-system
  :defsystem-depends-on (:asdf-package-system)
  :perform (test-op (o s) (cl-linter/tests/all:run-tests))
  :pathname #p"./tests/")
