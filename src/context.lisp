;;;-*- Mode:     Lisp -*-

(uiop/package:define-package :cl-linter/context
    (:use :cl)
  (:import-from :cl-linter/core
                #:define-context
                #:with-ctx
                #:ctx-check))

(in-package :cl-linter/context)

(define-context :defpackage
    :symbol 'defpackage)

(define-context :defun
    :symbol 'defun
    :traverse (lambda (form)
                (with-ctx (:args-ctx)
                  (ctx-check (third form)))
                (with-ctx (:func-body)
                  (ctx-check (cdddr form)))))

(define-context :defmethod
    :symbol 'defmethod)

(define-context :defclass
    :symbol 'defclass
    :traverse (lambda (form)
                (dolist (slot (fourth form))
                  (with-ctx (:slot-ctx)
                    (ctx-check slot)))
                (with-ctx (:class-options)
                  (ctx-check (fifth form)))))

(define-context :class-slot
    :name :slot-ctx)

(define-context :class-options
    :name :class-options)

(define-context :function-args
    :name :args-ctx)

(define-context :function-body
    :name :func-body)

(define-context :make-instance
    :symbol 'make-instance)
