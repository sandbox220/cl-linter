;;;-*- Mode:     Lisp -*-

(uiop/package:define-package :cl-linter/core
    (:use :cl)
  (:export #:define-ruleset
           #:define-rule
           #:with-ctx
           #:ctx-check
           #:lint-check
           #:linter-error))

(in-package :cl-linter/core)

(defparameter *ruleset* nil
  "List of ruleset names for current `define-rule' context")

(defparameter *contexts* (make-hash-table :test #'eq)
  "Map { context-name -> context-instance }")

(defparameter *ctx-stack* nil
  "Current contexts")

(defmacro define-ruleset ((name &rest options) &body body)
  (declare (ignore options))
  `(let ((*ruleset* (cons ,name *ruleset*)))
     ,@body))

(defmacro define-rule ((name context) (&rest args) &body body)
  (declare (ignore name))
  `(progn
     (assert *ruleset* ()
             "define-rule must be used inside define-ruleset context")
     (assert (gethash ,context *contexts*) ()
             "Context is not defined")
     (push (lambda (,@args &rest __args__)
             (declare (ignore __args__))
             ,@body)
           (ctx-rules (gethash ,context *contexts*)))))

(define-condition linter-error (error)
  ((message :initarg :message
            :type string
            :initform nil
            :reader le-message))
  (:report (lambda (err stream)
             (format stream "Linter error in context of ~A"
                     (car *ctx-stack*))
             (when (le-message err)
               (format stream ":~%  ~A" (le-message err))))))

(defclass basic-context ()
  ((name :initarg :name
          :reader ctx-name
          :type keyword
          :documentation "Unique name for an instance")
   (rules :initform nil
          :accessor ctx-rules
          :type list
          :documentation "List of all rules defined for current ctx"))
  (:documentation "Basic ctx implementation"))

(defclass dummy-context (basic-context)
  ()
  (:documentation "Dummy implementation for undefined contexts"))

(defconstant +dummy+ (make-instance 'dummy-context :name :dummy))

(defmethod print-obj ((obj basic-context) stream)
  (format stream "~A{~A}"
          (type-of obj) (ctx-name obj)))

(defgeneric $make-ctx (arg)
  (:method (arg) nil))

(defgeneric $ctx-traverse (ctx form)
  (:method (ctx form)
    (ctx-check form)
    (mapc #'check-form (cdr form))))

(defmacro define-context (name &rest options)
  `(progn
     (setf (gethash ,name *contexts*)
           (make-instance 'basic-context
                          :name ,name))
     ,(let ((getter (or (getf options :symbol)
                        (getf options :name))))
        (when getter
          `(defmethod $make-ctx ((arg (eql ,getter)))
             (gethash ,name *contexts*))))
     ,(let ((traverse (getf options :traverse)))
        (when traverse
          `(defmethod $ctx-traverse ((ctx (eql ,name)) form)
             (funcall ,traverse form))))))

(defmacro with-ctx ((ctx) &body body)
  `(let ((ctx-instance (when ,ctx ($make-ctx ,ctx))))
     (cond
       (ctx-instance
        (let ((*ctx-stack* (cons ctx-instance *ctx-stack*)))
          ,@body))
       ((eq +dummy+ (car *ctx-stack*))
        ,@body)
       (t (let ((*ctx-stack* (cons +dummy+ *ctx-stack*)))
            ,@body)))))

(defun lint-check (str)
  (let ((index 0))
    (labels ((%read ()
               (multiple-value-bind (form offset)
                   (read-from-string str nil nil :start index)
                 (setf index offset)
                 form)))
      (loop :for form := (%read)
         :while form
         :do (check-form form)))))

(defun ctx-check (form)
  (loop :for rule-fn :in (ctx-rules (car *ctx-stack*))
     :do (funcall rule-fn form)))

(defun $name ()
  (ctx-name (car *ctx-stack*)))

(defun check-form (form)
  (when (listp form)
    (with-ctx ((car form))
      ($ctx-traverse ($name) form))))
