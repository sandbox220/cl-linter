;;;-*- Mode:     Lisp -*-

(uiop/package:define-package :cl-linter/rules
    (:use :cl)
  (:import-from :cl-linter/context)
  (:import-from :cl-linter/core
                #:define-ruleset
                #:define-rule
                #:linter-error))

(in-package :cl-linter/rules)

(define-ruleset (:package)
  (define-rule (:defpackage :defpackage) ()
    (error 'linter-error
           :message "defpackage should not be used")))

(defparameter *slot-options-order*
  '(:accessor :reader :writer
    :initarg :initform :type
    :documentation))

(define-ruleset (:clos)
  (define-rule (:make-instance-typed :make-instance) (form)
    (let* ((arg1 (second form))
           (type (if (consp arg1)
                     ;; quote
                     (second arg1)
                     ;; TODO: handle symbols
                     nil)))
      (when type
        (handler-bind
            ((error (lambda (e)
                      (error 'linter-error
                             :message (with-output-to-string (str)
                                        (format str "~A" e))))))
          (typep t type)))))

  (define-rule (:typed-slot :class-slot) (form)
    (unless (getf (cdr form) :type)
      (error 'linter-error
             :message "No type defined for slot ~A" (car form))))

  (define-rule (:documented-slot :class-slot) (form)
    (unless (getf (cdr form) :documentation)
      (error 'linter-error
             :message "No documentation for slot ~A" (car form))))

  (define-rule (:documented-class :class-options) (form)
    (unless (getf form :documentation)
      (error 'linter-error
             :message "No documentation for class")))

  (define-rule (:slot-options-order :class-slot) (form)
    (labels ((%index (elem)
               (loop :for i :from 0
                  :for e :in (cdr form)
                  :when (eq e elem)
                  :return i
                  :finally (return nil))))
      (let ((opt-order
             (remove nil (mapcar #'%index *slot-options-order*))))
        (unless (apply #'< opt-order)
          (error 'linter-error
                 :message "Wrong order for slot options. Expected ~A"
                 *slot-options-order*))))))

(defparameter *max-args-count* 4)
(defparameter *max-optional-count* 1)

(define-ruleset (:codestyle)
  (define-rule (:args-count :function-args) (form)
    (let ((args-count
           (loop :for count :from 0
              :for arg :in form
              :while (not (member arg '(&optional &key &rest &aux)))
              :finally (return count))))
      (when (> args-count *max-args-count*)
        (error 'linter-error
               :message "Too many arguments. ~A allowed, ~A found"
               *max-args-count* args-count))))

  (define-rule (:optional-count :function-args) (form)
    (let* ((opt-args (cdr (member '&optional form)))
           (args-count
            (loop :for count :from 0
               :for arg :in opt-args
               :while (not (member arg '(&key &rest &aux)))
               :finally (return count))))
      (when (> args-count *max-optional-count*)
        (error 'linter-error
               :message "Too many &optional arguments. ~A allowed, ~A found"
               *max-optional-count* args-count))))

  (define-rule (:docstring :function-body) (form)
    (unless (stringp (car form))
      (error 'linter-error
             :message "Missing docstring for a function"))))
