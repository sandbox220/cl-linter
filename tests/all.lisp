(uiop/package:define-package :cl-linter/tests/all
    (:use :cl)
  (:import-from :cl-linter/tests/rules)
  (:import-from :cl-linter/tests/engine)
  (:export #:run-tests))

(in-package :cl-linter/tests/all)

(defun run-tests ()
  (cl-linter/tests/engine:run-tests))
