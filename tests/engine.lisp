(uiop/package:define-package :cl-linter/tests/engine
    (:use :cl)
  (:export #:run-tests
           #:define-test
           #:define-error-test))

(in-package :cl-linter/tests/engine)

(defparameter *test-list* nil)
(defparameter *disabled-tests* nil)
(defconstant +tests-per-line+ 40)

(defun register-test (name)
  (unless (member name *disabled-tests*)
    (pushnew name *test-list*)))

(defmacro define-test (name &body body)
  (register-test name)
  `(defun ,name ()
     ,@body
     t))

(defmacro define-error-test (name (&optional (condition 'error) handler)
                             &body body)
  (register-test name)
  `(defun ,name ()
     (handler-bind ((,condition (lambda (e)
                                  (declare (ignorable e))
                                  ,(when handler
                                       `(funcall ,handler e))
                                  (return-from ,name t))))
       ,@body
       (error "No error signaled"))))

(defmacro disable-test ((macro name &rest rest))
  (declare (ignore macro rest))
  (pushnew (if (consp name)
               (car name)
               name)
           *disabled-tests*)
  nil)

(defun run-tests ()
  (let ((count 0)
        (failed nil))
    (format *terminal-io* "RUNNING TESTS:~%")
    (dolist (test (reverse *test-list*))
      (if (handler-bind ((error (lambda (e)
                                  (declare (ignore e))
                                  nil)))
            (funcall test))
          (format *terminal-io* ".")
          (progn
            (push test failed)
            (format *terminal-io* "X")))
      (when (>= (incf count) +tests-per-line+)
        (setf count 0)
        (format *terminal-io* "~%")))
    (format *terminal-io* "~%PASSED: ~A/~A"
            (- (length *test-list*) (length failed))
            (length *test-list*))
    (when *disabled-tests*
      (format *terminal-io* "~%DISABLED:~{~%  ~A~}"
              *disabled-tests*))
    (when failed
        (format *terminal-io* "~%FAILED: ~A/~A~{~%  ~A~}"
                (length failed) (length *test-list*)
                failed))
    (null failed)))
