(uiop/package:define-package :cl-linter/tests/rules
	(:use :cl)
  (:import-from :cl-linter/rules)
  (:import-from :cl-linter/core
				#:linter-error
				#:lint-check)
  (:import-from :cl-linter/tests/engine
				#:define-error-test
				#:define-test
				#:disable-test))

(in-package :cl-linter/tests/rules)

(defmacro define-fail-test (name &body body)
  `(define-error-test ,name (linter-error)
	 ,@body))

(define-fail-test fail.defpackage.1
  (lint-check "(defpackage)"))

(define-fail-test fail.defpackage.2
  (lint-check "(progn (defpackage))"))

(define-fail-test fail.defpackage.3
  (lint-check "(foo (bar nil (defpackage)))"))

(define-fail-test fail.defpackage.qualified.1
  (lint-check "(cl:defpackage)"))

(define-fail-test fail.defpackage.qualified.2
  (lint-check "(cl-user::defpackage)"))

(define-fail-test fail.defpackage.qualified.3
  (lint-check "(cl-linter/tests/all::defpackage)"))

(define-test defpackage.1
  (lint-check "defpackage"))

(define-test defpackage.2
  (lint-check "(foo defpackage)"))

(define-test make-instance.1
  (lint-check "(make-instance 'cl-linter/core::basic-context)"))

(define-fail-test fail.make-instance.1
  (lint-check "(make-instance 'cl-linter/core::basic-context-42)"))

(define-test make-instance.2
  (lint-check "(let ((a 'cl-linter/core::basic-context))
                  (make-instance a))"))

(disable-test
 (define-fail-test fail.make-instance.2
   (lint-check "(let ((a 'cl-linter/core::basic-context-42))
                  (make-instance a))")))

(define-test args-count.1
  (lint-check "(defun foo (bar buz foobar foobuz))"))

(define-fail-test fail.args-count.1
  (lint-check "(defun foo (bar buz foobar foobuz fail))"))

(define-test args-count.2
  (lint-check "(defun foo (bar buz foobar foobuz &key ok test))"))

(define-test optional.count.1
  (lint-check "(defun foo (&optinal bar &key buz foobar foobuz))"))

(define-test optional.count.2
  (lint-check "(defun foo (bar &optional buz))"))

(define-test optional.count.3
  (lint-check "(defun foo (&optional bar &aux))"))

(define-fail-test fail.optional.count.1
  (lint-check "(defun foo (&optional bar buz))"))

(define-fail-test fail.optional.count.2
  (lint-check "(defun foo (foobar &optional bar buz))"))

(define-fail-test fail.optional.count.3
  (lint-check "(defun foo (&optional bar buz &rest))"))

(define-fail-test fail.optional.count.4
  (lint-check "(defun foo (&key key &optional bar buz &rest rest))"))
